import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './layout/main/main.component';
import { AuthComponent } from './login/auth/auth.component';

import { TestComponent } from './dragdrop/test/test.component';

const routes: Routes = [
    // login 不會有 header footer
    {
      path: 'login',
      loadChildren: './login/login.module#LoginModule'
    }
    ,
  {
    path: '',
    component: MainComponent,
    // 定義在子路由  所以都會套版到 MainComponent 的 header footer
    loadChildren: './posts/posts.module#PostsModule'
  },

  // login 不會有 header footer
  {
    path: 'test',
    component: TestComponent
  }
  ,
  {
    // 萬用路由 都不符合時預設路徑
    path: '**',
    // redirectTo 要用full
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
