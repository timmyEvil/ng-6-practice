import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgModel } from '@angular/forms';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {


  @ViewChild('email') email: NgModel;
  @ViewChild('emailDom') emailDom: ElementRef;

  loginData = {
    pass: '',
    email: ''
  };

  // 路由設定調整
  constructor(private router: Router) { }

  ngOnInit() {
    // this.emailDom.nativeElement;
  }

  doLogin() {
    // ...do login
    // this.router.navigateByUrl('/posts');

    this.router.navigate(['/', 'create'], {
      queryParams: {
        foo: 'bar'
      }
    });
  }

}
