import { DragdropModule } from './dragdrop.module';

describe('DragdropModule', () => {
  let dragdropModule: DragdropModule;

  beforeEach(() => {
    dragdropModule = new DragdropModule();
  });

  it('should create an instance', () => {
    expect(dragdropModule).toBeTruthy();
  });
});
