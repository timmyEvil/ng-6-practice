import { Component, OnDestroy, OnInit } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit, OnDestroy {

  DRAGULA_AREA = 'DRAGULA_AREA';

  id;

  a1 = [
    { text: '標籤', value: 'label', inputValue: 'o1' },
    { text: '換行', value: 'br', inputValue: 'o2' },
    { text: '文數字', value: 'strNum', inputValue: 'o3' }
  ];

  list = [
    {
      question: '您今年因為何事做筆錄？',
      answer: [
        { text: '標籤', value: 'label', inputValue: 'a1' },
        { text: '換行', value: 'br', inputValue: 'a2' },
        { text: '文數字', value: 'strNum', inputValue: 'a3' }
      ]
    },
    {
      question: '發生甚麼事？',
      answer: [
        { text: '標籤', value: 'label', inputValue: 'b1' },
        { text: '換行', value: 'br', inputValue: 'b2' },
        { text: '文數字', value: 'strNum', inputValue: 'b3' }
      ]
    }
  ];

  subs = new Subscription();


  public constructor(
    private dragulaService: DragulaService) {

    dragulaService.createGroup(this.DRAGULA_AREA, {
      copy: (el, source) => {
        return source.id === 'source';
      },
      copyItem: (item: any) => {
        return { text: item.text, value: item.value, inputValue: item.inputValue };
      }
    });



    // this.subs.add(dragulaService.out(this.DRAGULA_FACTS)
    //     .subscribe(({ el, container }) => {
    //         console.log('out', container);
    //         dragulaService.remove();
    //     })
    // );

    // this.subs.add(dragulaService.dropModel(this.DRAGULA_TRASH)
    //     .subscribe(({ el, target, source, sourceModel, targetModel, item }) => {
    //         console.log('DRAGULA_TRASH dropModel:');
    //         console.log('el:');
    //         console.log(el);

    //         el.remove();
    //     })
    // );

    // this.subs.add(dragulaService.out(this.DRAGULA_AREA)
    //     .subscribe(({ el, container }) => {
    //         console.log('out', container);
    //         this.removeClass(container, 'ex-over');
    //     })


    this.subs.add(dragulaService.dropModel(this.DRAGULA_AREA)
      .subscribe(({ el, target, source, sourceModel, targetModel, item }) => {
        console.log('dropModel:');
        console.log(el);
        console.log(source);
        console.log(target);
        console.log(sourceModel);
        console.log(targetModel);
        console.log(item);
        // if (target.id === 'trashArea') {
        //     console.log('remove!');
        //     el.remove();
        // }
      })
    );
    this.subs.add(dragulaService.removeModel(this.DRAGULA_AREA)
      .subscribe(({ el, source, item, sourceModel }) => {
        console.log('removeModel:');
        console.log(el);
        console.log(source);
        console.log(sourceModel);
        console.log(item);
      })
    );
  }

  ngOnInit() {
    console.log('init');
  }

  remove(answer: any[], index) {
    if (index > -1) {
      answer.splice(index, 1);
    }
  }


  public close() {

    this.dragulaService.destroy(this.DRAGULA_AREA);

  }

  ngOnDestroy() {
    // destroy all the subscriptions at once
    this.subs.unsubscribe();
  }
}
