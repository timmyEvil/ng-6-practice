import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from '@angular/forms';
import { debounceTime, takeUntil, switchMap } from 'rxjs/operators';
import { Subscription, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit, OnDestroy {

  post = new FormGroup({
    title: new FormControl('抬頭', Validators.required),
    body: new FormControl(null, [Validators.required, Validators.minLength(10)]),
    tags: new FormArray([
      new FormControl('Angular'),
      new FormControl('HTML'),
      new FormControl('CSS')
    ]),
    seo: new FormGroup({
      meta: new FormControl('angular')
    })
  });

  get tags(): FormArray {
    return this.post.get('tags') as FormArray;
  }

  get title(): AbstractControl {
    return this.post.get('title');
  }

  get body(): AbstractControl {
    return this.post.get('body');
  }
  constructor(private httpClient: HttpClient) { }

  destroy$ = new Subject();
  operator: Subscription;

  ngOnInit() {


    this.post.get('title').valueChanges.subscribe(v => {
      console.log(v);
    });

    this.operator = this.post.get('title').valueChanges
    .pipe(
      takeUntil(this.destroy$),
      debounceTime(300),
      // switchMap(this.httpClient.get(`http://some/article/${post.catId}`))
    ).subscribe(v => {

    });

    // fromPromise 轉成 operators
    // debounceTime();
    // toPromise 不要用吧!
  }

  addTag(tag) {
    if (tag) {
      this.tags.push(new FormControl(tag));

    }
  }

  removeTag(index) {
    this.tags.removeAt(index);
  }

  createPost() {
    console.log(this.post.value);
  }


  test() {
    // formEvent
  }


  ngOnDestroy() {
    this.destroy$.next();
  }
}
