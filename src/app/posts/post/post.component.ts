import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  postId: number;



  constructor(private route: ActivatedRoute, private httpClient: HttpClient) { }

  // const getCat = params => this.httpClient.get('http://');

  // const getMapToPost = (post: any) =>
  // this.httpClient.get('http://');

  ngOnInit() {
    this.route.params.subscribe(query => {
      console.log(query);
      this.postId = query['id']; // id 是 路由路徑 post/:id定的
    });

    this.route.queryParams.subscribe(query => {
      console.log(query);
    });



    // this.route.params.pipe(
    //   switchMap(getCat),
    //   switchMap(getCat)
    // )

  }

}
